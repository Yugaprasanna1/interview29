import datetime
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from pymongo import MongoClient
import urllib.parse
import time

# Your MongoDB Atlas connection string
username = 'cbadmin'
password = 'cab@123'
escaped_username = urllib.parse.quote_plus(username)
escaped_password = urllib.parse.quote_plus(password)
connection_string = f"mongodb+srv://{escaped_username}:{escaped_password}@atlascluster.bg29nup.mongodb.net/?retryWrites=true&w=majority&appName=AtlasCluster"

# Initialize the MongoDB client
client = MongoClient(connection_string)

# Access the database
db = client["CloudAndBeyond"]

# Access the collection
collection = db["dataStorage"]

# Email configuration
smtp_server = 'smtp.gmail.com'
smtp_port = 587
sender_email = 'yuga81d9997@gmail.com'
sender_password = 'ghyf ssdh vumw kieb'

# Function to send interview email
def send_interview_email(position_applied_for, candidate_email, vendor_email, schedule_time, meeting_time, meeting_link, meeting_id):
    # Create a multipart message
    msg = MIMEMultipart()
    msg['From'] = sender_email
    msg['To'] = candidate_email
    msg['Cc'] = vendor_email
    msg['Subject'] = 'Interview Schedule'

    # Email body
    body = f"Dear Candidate and Vendor,\n\nYou have an interview scheduled for the position of {position_applied_for}.\n" \
           f"The meeting will start at {meeting_time}. " \
           f"Please use the following link to join the meeting: {meeting_link}\n\n" \
           f"Regards,\nYour Organization"
    msg.attach(MIMEText(body, 'plain'))

    # Connect to SMTP server and send email
    try:
        with smtplib.SMTP(smtp_server, smtp_port) as server:
            server.starttls()
            server.login(sender_email, sender_password)
            server.sendmail(sender_email, [candidate_email, vendor_email], msg.as_string())
        print("Interview email sent.")
        return True
    except Exception as e:
        print(f"Failed to send emails. Error: {e}")
        return False

# Function to retrieve data from MongoDB and send interview email at scheduled time
def send_emails_at_scheduled_time():
    # Get current time
    current_time = datetime.datetime.now().time()

    # Get all documents from the collection where ScheduleTime matches the current time
    documents = list(collection.find({"ScheduleTime": current_time.strftime('%H:%M')}))
    
    # Check if there are any matching documents
    if len(documents) == 0:
        print("No documents found for the current scheduled time.")
        return False

    # Iterate through documents
    for doc in documents:
        position_applied_for = doc.get('PositionAppliedFor')
        candidate_email = doc.get('CandidateMailID')
        vendor_email = doc.get('VendorMailID')
        schedule_time = doc.get('ScheduleTime')  # Retrieve schedule time
        meeting_time = doc.get('Meeting_Time')  # Retrieve meeting time
        meeting_link = doc.get('MeetingLink')
        meeting_id = doc.get('MeetingID')

        # Send interview email
        if send_interview_email(position_applied_for, candidate_email, vendor_email, schedule_time, meeting_time, meeting_link, meeting_id):
            return True
        else:
            return False

if __name__ == "__main__":
    # Loop indefinitely to check for scheduled emails
    while True:
        # Check and send emails at scheduled time
        if send_emails_at_scheduled_time():
            break  # Exit the loop if email is sent successfully
        else:
            time.sleep(60)  # Check every minute if scheduled email needs to be sent
